variable "proj" {
   description = "projectname"
   type = string
   default = "accubits"
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    type = string
    default = "10.0.0.0/16"
}
variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    type = string
    default = "10.0.2.0/24"
}

variable "private_subnet_cidr" {
    description = "CIDR for the Private Subnet"
    type = string
    default = "10.0.1.0/24"
}
variable "int_subnet_cidr" {
    description = "Internet to webserver"
    type = string
    default = "0.0.0.0/0"
}

variable "ec2_ami" {
    description = "AMIs by region"
    default = "ami-0ae8c80279572fa66"
}
variable "ipcidr" {
    type = string
    default = "32"
}
variable "http_port" {
    description = "webserver port"
    default = "80"
}
variable "https_port" {
    description = "secure webserver port"
    default = "443"
}
variable "mysql_port" {
   description = "database port"
   default = "3306"
}


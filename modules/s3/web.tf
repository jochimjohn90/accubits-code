resource "aws_s3_bucket" "Acc2021" {
  bucket = "Acc2021"
  acl    = "public-read"
  region  =  "eu-west-2"
  website {
    index_document = "index.html"
    error_document = "error.html"
  }
  tags = {
    Name        = "Accubits2021"
  }
}

resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = "$(aws_s3_bucket.Acc2021.bucket_regional_domain_name)"
    origin_id   = "${local.s3_origin_id}"
  }
  enabled             = true
  is_ipv6_enabled     = true
  comment             = abc
  default_root_object = "index.html"

  viewer_certificate {
  cloudfront_default_certificate = true
  }
}


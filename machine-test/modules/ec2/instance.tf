data "template_file" "ec2acceskey" {
  template = "${file("/home/accubits/.ssh/id_rsa")}"
}
resource "aws_key_pair" "accubits-key" {
  key_name   = var.proj
  public_key = "${data.template_file.ec2acceskey.rendered}"
}
resource "aws_vpc" "accubits_vpc" {
    cidr_block = var.vpc_cidr
    enable_dns_hostnames = true
    tags = {
        Name = "Accubits-aws-vpc"
    }
}

resource "aws_subnet" "accubits-pub" {
    vpc_id = "${aws_vpc.accubits_vpc.id}"
    cidr_block = var.public_subnet_cidr
    tags = {
        Name = "Accubits-Public-Subnet"
    }
}

resource "aws_subnet" "accubits-pvt" {
    vpc_id = "${aws_vpc.accubits_vpc.id}"
    cidr_block = var.private_subnet_cidr
    tags = {
        Name = "Accubits-Private-Subnet"
    }
}

resource "aws_internet_gateway" "accubits_igw" {
    vpc_id = "${aws_vpc.accubits_vpc.id}"
    tags = {
        Name = "Accubits-Igw"
  }
}

resource "aws_route_table" "pubrt" {
    vpc_id = "${aws_vpc.accubits_vpc.id}"
    route {
        cidr_block = var.int_subnet_cidr
        gateway_id = "${aws_internet_gateway.accubits_igw.id}"
    }
    tags = {
        Name = "Accubits-Public-Subnet-Route"
    }
}

resource "aws_route_table_association" "routepub" {
    subnet_id = "${aws_subnet.accubits-pub.id}"
    route_table_id = "${aws_route_table.pubrt.id}"
}

resource "aws_eip" "NGW" {
    vpc = true
}

resource "aws_nat_gateway" "accubits-ngw" {
    allocation_id = "${aws_eip.NGW.id}"
    subnet_id     = "${aws_subnet.accubits-pub.id}"
    depends_on = ["aws_internet_gateway.accubits_igw"]
    tags = {
        Name = "Accubits-Ngw"
  }
}

resource "aws_route_table" "pvtrt" {
    vpc_id = "${aws_vpc.accubits_vpc.id}"

    route {
        cidr_block = var.int_subnet_cidr
        gateway_id = "${aws_nat_gateway.accubits-ngw.id}"
    }

    tags = {
        Name = "Accubits-Private-Subnet-Route"
    }
}

resource "aws_route_table_association" "routepvt" {
    subnet_id = "${aws_subnet.accubits-pvt.id}"
    route_table_id = "${aws_route_table.pvtrt.id}"
}

resource "aws_security_group" "public_sg" {
    name = "vpc_public_sg"
    description = "Allow traffic to pass from the internet to public servers"
    ingress {
        from_port = var.http_port
        to_port = var.http_port
        protocol = "tcp"
        cidr_blocks = var.int_subnet_cidr
    }
    ingress {
        from_port = var.https_port
        to_port = var.https_port
        protocol = "tcp"
        cidr_blocks = var.int_subnet_cidr
    }
    ingress {
        from_port = var.http_port
        to_port = var.http_port
        protocol = "tcp"
        ipv6_cidr_blocks = ["::/0"]
    }
    ingress {
        from_port = var.https_port
        to_port = var.https_port
        protocol = "tcp"
        ipv6_cidr_blocks = ["::/0"]
    }

   vpc_id = "${aws_vpc.accubits_vpc.id}"

   tags = {

     Name = "Accubits-Public-SG"

   }
}

resource "aws_security_group" "private_sg" {
    name = "private_sg"
    description = "Allow traffic to pass from the public subnet to rds instance"
    ingress {
        from_port = var.mysql_port
        to_port = var.mysql_port
        protocol = "tcp"
        cidr_blocks = var.public_subnet_cidr
    }
  vpc_id = "${aws_vpc.accubits_vpc.id}"

  tags = {
       Name = "Accubits-Private-SG"
  }
}

resource "aws_eip" "prim" {
  instance = "${aws_instance.primary.id}"
  vpc = true
}

resource "aws_eip" "sec" {
  instance = "${aws_instance.secondary.id}"
  vpc = true
}

resource "aws_instance" "primary" {
  ami = var.ec2_ami
  instance_type = "t2.nano"
  subnet_id = "${aws_subnet.accubits-pub.id}"
  vpc_security_group_ids = "${aws_security_group.public_sg.id}"
  key_name = "accubits-key"
  associate_public_ip_address = true
  source_dest_check = false
  tags = {
           Name = "Accubits-Server01"
  }
}

resource "aws_instance" "secondary" {
  ami = var.ec2_ami
  instance_type = "t2.nano"
  subnet_id = "${aws_subnet.accubits-pub.id}"
  vpc_security_group_ids = "${aws_security_group.public_sg.id}"
  key_name = "accubits-key"
  associate_public_ip_address = true
  tags = {
       Name = "Accubits-Server02"
  } 
}

resource "aws_elb" "acc-elb" {
  name               = "accubits-terraform-elb"
  availability_zones = ["us-west-2a"]
  vpc_id = "${aws_vpc.accubits_vpc.id}"
  

  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  listener {
    instance_port      = 8080
    instance_protocol  = "http"
    lb_port            = 443
    lb_protocol        = "https"
    ssl_certificate_id = "arn:aws:iam::4653458845012:server-acb/ssl"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8080/"
    interval            = 30
  }

  instances                   = ["aws_instance.primary.id", "aws_instance.primary.id"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "accubits-terraform-elb"
  }
}

resource "aws_db_instance" "Accubits-RDS" {
  allocated_storage    = "100"
  engine               = "mysql"
  engine_version       = "8.0"
  instance_class       = "db.t3.medium"
  database_name        = "accubitsdb"
  master_username      = "accdb"
  master_password      = "QdfguyuEd@sdgt21"
  parameter_group_name = "default.mysql8.0"
  availability_zones   = "us-west-2a"
  subnet_id = "${aws_subnet.accubits-pvt.id}" 
  vpc_security_group_ids = "${aws_security_group.public_sg.id}"
  associate_public_ip_address = false
  tags = {
       Name = "Accubits-DB-Server"
  }
}

